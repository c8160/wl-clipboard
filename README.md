# wl-clipboard

A command-line utility to interact with the Wayland clipboard.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is wl-clipboard?

wl-clipboard implements two command-line Wayland clipboard utilities, wl-copy
and wl-paste, that let you easily copy data between the clipboard and Unix
pipes, sockets, files and so on.

See [the project website](https://github.com/bugaevc/wl-clipboard)


## How to use this image

This image is meant to be a one-to-one replacement of a natively installed
`wl-clipboard`. Use it like this:

```bash
$ podman run --rm -i \
    --userns keep-id \
    --log-driver none \
    -v "$XDG_RUNTIME_DIR/wayland-0:$XDG_RUNTIME_DIR/wayland-0" \
    -e XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" \
    --security-opt label=disable \
    --entrypoint /usr/bin/wl-copy \
    registry.gitlab.com/c8160/wl-clipboard:latest
```

The options have the following meanings:
- `--userns keep-id`: Access the wayland socket as the user of the current
  session
- `--log-driver none`: Do not write container logs to systemd
- `-v "$XDG_RUNTIME_DIR/wayland-0:$XDG_RUNTIME_DIR/wayland-0"`: Mount in the
  wayland socket
- `-e XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR"`: Tell `wl-clipboard` where the
  wayland socket resides
- `--security-opt label=disable`: Disable SELinux labels that prevent access to
  the wayland socket for containers

The CI publishes images with the default entrypoint set to `wl-copy`. If you
need access to `wl-paste`, change the call to the container above to contain
`wl-paste` instead of `wl-copy`.

## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/wl-clipboard/-/issues

