wl-clipboard image, for interacting with the wayland clipboard.

Packages the [wl-clipboard Github project](https://github.com/bugaevc/wl-clipboard),
in particular the `wl-copy` and `wl-paste` executables.

Volumes:
$XDG_RUNTIME_DIR/wayland-0: Mapped to the same location in the container. This
  is the wayland socket that manages the clipboard. `wl-clipboard` will not
  work without this.

Environment variables:
XDG_RUNTIME_DIR: Set to `$XDG_RUNTIME_DIR` of the host this container is run
  on. Otherwise `wl-clipboard` doesn't know where to search the wayland socket
  and will not work.

Documentation:
For the underlying wl-clipboard project, see https://github.com/bugaevc/wl-clipboard
For this container, see https://gitlab.com/c8160/wl-clipboard

Requirements:
Works only with Wayland, no X11 support.

Configuration:
None

